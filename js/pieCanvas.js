/**
 * Pie Chart on canvas
 * @author Tokiroto (StanBir)
 * @param {String} canvasId
 * @param {Object} options - {
 * - separation { type - "line", "";
 *              , width - width of line
 *              , color - color of the line
 *              }
 * - is3D - boolean mode, in 3d draws orthographic look
 * - colors : {
 *    empty - color for empty pie
 *    hightlight - color for hightlighting
 * },
 * - separation - object with params about embosing of parts {
 *     type - string "line"
 *     color - color as "red, green..." or "#202020"
 *     width - default is 1
 * }
 * @returns {PieChart.pieCanvasAnonym$6}
 */
var PieChart = (function (canvasId, options) {
    var canvas = document.getElementById(canvasId),
            ctx = canvas.getContext("2d"),
            settings = {
                cW: canvas.width,
                cH: canvas.height,
                pieParts: [],
                is3D: false,
                separation: {
                },
                bevelValue: 40,
                scaleFactor: 0.7,
                colors: {
                    empty: "grey",
                    hightlight: "0x1a1a1a"
                },
                startAngle: 0,
                radius: 100,
                totalAngle: 0,
                x: (canvas.width) ? Math.floor(canvas.width / 2) : canvas.width,
                y: (canvas.height) ? Math.floor(canvas.height / 2) : canvas.height
            };
    settings = Object.assign(settings, options);
    // center the drawing
    ctx.translate(settings.x / settings.radius, settings.y / settings.radius);
    if (settings.debug) {
        var debug = settings.debug;
        if (debug.xId) {
            var $xPosText = document.getElementById(debug.xId);
        }
        if (debug.yId) {
            var $yPosText = document.getElementById(debug.yId);
        }
        if (debug.valId) {
            var $valText = document.getElementById(debug.valId);
        }
    }
    /* check for callback defition for tooltip */
    if (settings.tooltip) {
        if (typeof settings.tooltip.move !== "function") {
          settings.tooltip.move = moveTooltip;
        }
        if (typeof settings.tooltip.show !== "function") {
          settings.tooltip.show = showTooltip;
        }
        if (typeof settings.tooltip.hide !== "function") {
          settings.tooltip.hide = hideTooltip;
        }
        if (typeof settings.tooltip.cursorOffset !== "object") {
          settings.tooltip.cursorOffset = {
            x: 10,
            y: 10
          };
        }
        // find element and cache it
        settings.tooltip.tooltipContainer = document.getElementById(settings.tooltip.id);
        if (settings.tooltip.tooltipContainer !== null) {
          settings.hasTooltip = true;
        }
    }
    if (settings.is3D) {
        ctx.scale(1, settings.scaleFactor);
        settings.y /= settings.scaleFactor;
    }
    canvas.onmousemove = onMouseMove;
    settings = initPieData(settings);

    /**
     * Calculate Pie parts data
     * Calculates Pie Parts data for start angle, end angle.
     *
     * @returns {Object}
     */
    function initPieData() {
        if (!!settings && !!settings.pieParts) {
            var pieParts = settings.pieParts;
            settings.angles = [];
            settings.totalValues = 0;
            for (var i = 0, length = pieParts.length; i < length; i++) {
                settings.totalValues += pieParts[i].val;
            }
            for (var i = 0, length = pieParts.length; i < length; i++) {
                var piePart = pieParts[i];
                var percent = piePart.val / settings.totalValues;
                var tAngle = (percent * (Math.PI * 2));
                var fAngle = settings.totalAngle;
                settings.angles[i] = {from: fAngle, to: fAngle + tAngle};
                settings.totalAngle += tAngle;
            }
        }
        return settings;
    }

    /**
     * Mouse move handler
     *
     * @param {Event} e
     * @private
     */
    function onMouseMove(e) {
        var boundRect = this.getBoundingClientRect();
        var pX = e.clientX - boundRect.left;
        var pY = e.clientY - boundRect.top;
        displayDebug($xPosText, pX);
        displayDebug($yPosText, pY);
        var distance = 0;
        var isInside = false;
        if (settings.is3D) {
            pY /= settings.scaleFactor;
            if (pY >= settings.y) {
                var distance = Math.sqrt(Math.pow(settings.x - pX, 2) + Math.pow(settings.y - pY, 2));
                var distance2 = Math.sqrt(Math.pow(settings.x - pX, 2) + Math.pow(settings.y - pY + settings.bevelValue, 2));
                isInside = (distance <= settings.radius || distance2 <= settings.radius);
            } else {
                distance = Math.sqrt(Math.pow(settings.x - pX, 2) + Math.pow(settings.y - pY, 2));
                isInside = (distance <= settings.radius);
            }
        } else {
            distance = Math.sqrt(Math.pow(settings.x - pX, 2) + Math.pow(settings.y - pY, 2));
            isInside = (distance <= settings.radius);
        }
        if (isInside) {
            var firstLayerIndex = getIndex(pX, pY);

            if (pY / settings.scaleFactor >= settings.y / settings.scaleFactor) {
                var intersection = intersectionIndex(pX, pY, pX < settings.x);
                firstLayerIndex = (intersection === -1) ? firstLayerIndex : intersection;
            }

            // calculate new angle
            if ((firstLayerIndex >= 0)) {
                var piePart = JSON.parse(JSON.stringify(settings.pieParts[firstLayerIndex])),
                        color = piePart.color.replace("#", ""),
                        lighterColor = getLighterColor(parseInt(color, 16), parseInt(settings.colors.hightlight, 16));
                piePart.color = "#" + lighterColor.toString(16);
                settings.selected = {piePart: piePart, index: firstLayerIndex};
                drawPie();
                displayDebug($valText, piePart.val);
                if (settings.hasTooltip) {
                    settings.tooltip.move(e.clientX, e.clientY);
                    settings.tooltip.show(piePart.color, Math.round(piePart.val / settings.totalValues * 100));
                }
            }
        } else {
            if (settings.selected && Object.keys(settings.selected).length) {
                settings.selected = {};
                drawPie();
            }
            displayDebug($valText, "");
            if (settings.hasTooltip) {
                settings.tooltip.hide();
            }
        }
    }

    function getLighterColor(color, hightlightDelta) {
        // if the lighterColor exids maximum
        // then left 4 digits from hightlight color
        var lighterColor = color + hightlightDelta;
        var step = 0xff;
        while (lighterColor >= 0xffffff) {
            lighterColor = color + parseInt(hightlightDelta & step);
            step << 8;
        }
        return lighterColor;
    }

    /**
     * Display debug information
     *
     * @param {Object} $item - container for debug info
     * @param {String} text - text for debug info
     * @private
     */
    function displayDebug($item, text) {
        if ($item) {
            $item.innerText = text;
        }
    }

    /**
     * Draw pie chart
     *
     * @public
     */
    function draw() {
        drawPie();
    }

    /**
     * Get index of pie part per mouseX and mouseY
     *
     * @param {Number} pX - mouse coordinates by X
     * @param {Number} pY - mouse coordinates by Y
     * @returns {Number} - index if found, else -1
     */
    function getIndex(pX, pY) {
        var cX = settings.x,
                cY = settings.y,
                r = settings.radius,
                pieParts = settings.pieParts;
        for (var i = 0, length = pieParts.length; i < length; i++) {
            var angle = settings.angles[i];
            var x1 = cX + r * Math.cos(angle.to);
            var y1 = cY + r * Math.sin(angle.to);
            var x2 = cX + r * Math.cos(angle.from);
            var y2 = cY + r * Math.sin(angle.from);
            var inside = pointInTriangle(
                    {
                        x: pX,
                        y: pY
                    },
                    {
                        x: cX,
                        y: cY
                    },
                    {
                        x: x1,
                        y: y1
                    },
                    {
                        x: x2,
                        y: y2
                    });
            if (inside) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Calculate intersection index for 3D mode
     *
     * @param {Number} pX - mouse coordinates by X
     * @param {Number} pY - mouse coordinates by Y
     * @param {Boolean} opposite - before middle by x, or after. We need this as we mirror triangle
     * @returns {Number}
     */
    function intersectionIndex(pX, pY, opposite) {
        var cX = settings.x,
                cY = settings.y,
                r = settings.radius,
                pieParts = settings.pieParts;
        for (var i = 0, length = pieParts.length; i < length; i++) {
            var angle = JSON.parse(JSON.stringify(settings.angles[i]));
            // if before middle by x, then we need to calculate from angle,
            // otherwise we calcute to angle
            var x1 = cX + r * Math.cos(opposite ? angle.from : angle.to);
            var y1 = cY + r * Math.sin(opposite ? angle.from : angle.to);
            var x2 = x1;
            var y2 = y1 + settings.bevelValue;
            var x3 = opposite ? x1 - settings.bevelValue : x1 + settings.bevelValue;
            var y3 = y2;
            var inside = pointInTriangle(
                    {
                        x: pX,
                        y: pY
                    },
                    {
                        x: x1,
                        y: y1
                    },
                    {
                        x: x2,
                        y: y2
                    },
                    {
                        x: x3,
                        y: y3
                    }, true);
            if (inside) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Apply current styling for pie chart
     *
     * @private
     */
    function applyAvailableStyling() {
        if (settings.separation) {
            var separation = settings.separation;
            switch (separation.type) {
                case "line":
                    ctx.strokeStyle = separation.color;
                    ctx.lineWidth = separation.width;
                    ctx.stroke();
                    break
                default:
                    break;
            }
        }
    }

    /**
     * Highlight pie part
     *
     * @private
     */
    function highlighPart() {
        if (settings.selected) {
            var selected = settings.selected,
                    i = selected.index,
                    angles = settings.angles[i];
            if (settings.is3D) {
                draw3DPiePart(ctx, settings.x, settings.y, settings.radius, selected.piePart.color, angles.from, angles.to, settings.bevelValue);
            } else {
                drawPiePart(ctx, settings.x, settings.y, settings.radius, selected.piePart.color, angles.from, angles.to);
            }
        }
    }

    /**
     *
     * @param {Number} i
     * @private
     */
    function unhighlight(i) {
        var angles = settings.angles[i],
                piePart = settings.pieParts[i];
        if (settings.is3D) {
            draw3DPiePart(ctx, settings.x, settings.y, settings.radius, piePart.color, angles.from, angles.to, settings.bevelValue);
        } else {
            drawPiePart(ctx, settings.x, settings.y, settings.radius, piePart.color, angles.from, angles.to);
        }
    }

    /**
     * Draw pie
     *
     * @private
     */
    function drawPie() {
        // clean the canvas before redrawing, as without cleaning it will lead to a messy curvature
        ctx.clearRect(0, 0, canvas.width * 2, canvas.height * 2);
        if (!!settings) {
            var cX = settings.x,
                cY = settings.y,
                r = settings.radius;
            if (!!settings.pieParts && settings.pieParts.length) {
                var pieParts = settings.pieParts;
                for (var i = 0, length = pieParts.length; i < length; i++) {
                    var piePart = pieParts[i];
                    if (settings.selected && Object.keys(settings.selected).length && settings.selected.index === i) {
                        piePart = settings.selected.piePart;
                    }
                    var color = piePart.color;
                    if (settings.is3D) {
                        draw3DPiePart(ctx, cX, cY, r, color, settings.angles[i].from, settings.angles[i].to, settings.bevelValue);
                    } else {
                        drawPiePart(ctx, cX, cY, r, color, settings.angles[i].from, settings.angles[i].to);
                    }
                }
            } else {
                var emptyColor = settings.colors.empty;
                if (settings.is3D) {
                    draw3DPiePart(ctx, cX, cY, r, emptyColor, settings.startAngle, 2 * Math.PI, settings.bevelValue);
                } else {
                    drawPiePart(ctx, cX, cY, r, emptyColor, settings.startAngle, 2 * Math.PI);
                }
            }
        }
    }

    /**
     * Draw pie chart part
     *
     * @param {Object} ctx - context of drawing aria
     * @param {Number} cX - center position X
     * @param {Number} cY - center position Y
     * @param {Number} r  - radius of the circle
     * @param {String} color - hex color of a pie piece e.g. "#231392"
     * @param {Number} fAngle - from angle value
     * @param {Number} tAngle - to angle value
     * @private
     */
    function drawPiePart(ctx, cX, cY, r, color, fAngle, tAngle) {
        ctx.save();
        ctx.beginPath();
        ctx.moveTo(cX, cY);
        ctx.arc(cX, cY, r, fAngle, tAngle);
        ctx.closePath();
        ctx.fillStyle = color;
        ctx.fill();
        applyAvailableStyling();
        ctx.restore();
    }

    /**
     * Draw 3D pie chart ( orthographic look )
     *
     * @param {Object} ctx
     * @param {Number} cX
     * @param {Number} cY
     * @param {Number} r
     * @param {String} color
     * @param {Number} fAngle
     * @param {Number} tAngle
     * @param {Number} bevelValue
     * @private
     */
    function draw3DPiePart(ctx, cX, cY, r, color, fAngle, tAngle, bevelValue) {
        var x1 = cX + r * Math.cos(fAngle),
                y1 = cY + r * Math.sin(fAngle),
                x2 = cX + r * Math.cos(tAngle),
                y2 = cY + r * Math.sin(tAngle),
                colorVal = color.replace("#", ""),
                hexColor = "#" + (parseInt(colorVal, 16) - 0x20EE).toString(16);
        ctx.save();
        if (y1 >= cY) {
            ctx.beginPath();
            ctx.fillStyle = hexColor;
            ctx.moveTo(cX, cY);
            if (y2 >= cY) {
                ctx.lineTo(x1, y1);
                ctx.lineTo(x1, y1 + bevelValue);
                ctx.arc(cX, cY + bevelValue, r, fAngle, tAngle);
                ctx.lineTo(x2, y2);
            } else {
                ctx.lineTo(cX - r, cY);
                ctx.lineTo(cX - r, cY + bevelValue);
                ctx.arc(cX, cY + bevelValue, r, Math.PI, fAngle, true);
                ctx.lineTo(x1, y1 + bevelValue);
                ctx.lineTo(x1, y1);
                ctx.lineTo(cX, cY);
            }
            ctx.fill();
            applyAvailableStyling();
        }
        ctx.restore();
        drawPiePart(ctx, cX, cY, r, color, fAngle, tAngle);
    }

    /**
     * Calculate poine for the triangle
     *
     * @param {Object} p1 - {x:..., y:...} values
     * @param {Object} p2 - {x:..., y:...} values
     * @param {Object} p3 - {x:..., y:...} values
     * @returns {Number}
     * @private
     */
    function sign(p1, p2, p3)
    {
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
    }

    /**
     * Indetify point in the triangle
     *
     * @param {Object} pt - {x:..., y:...} values
     * @param {Object} v1 - {x:..., y:...} values
     * @param {Object} v2 - {x:..., y:...} values
     * @param {Object} v3 - {x:..., y:...} values
     * @param {Boolean} allSides - count three size if true, otherwise only left, right, but not a bottom
     * @returns {Boolean} - true if point in the triangle, otherwise false
     * @private
     */
    function pointInTriangle(pt, v1, v2, v3, allSides)
    {
        var b1, b2, b3;

        b1 = sign(pt, v1, v2) < 0.0;
        /*
         Because the top point of the triagnle always points
         to the core of a pie chart, we just need to check left and right border,
         but not the bottom, so it always will be true.
         */
        b2 = (allSides) ? sign(pt, v2, v3) < 0.0 : true;
        b3 = sign(pt, v3, v1) < 0.0;

        return ((b1 === b2) && (b2 === b3));
    }

    /**
     * Default show tooltip
     *
     * @param {String} color - color in hex
     * @param {String} value - value to display in tooltip
     * @private
     */
    function showTooltip(color, value) {
        var colorDiv = this.tooltipContainer.querySelector("#color"),
            descriptionDiv = this.tooltipContainer.querySelector("#description");
        if (colorDiv.style.backgroundcolor !== color) {
            colorDiv.style.cssText = "background-color:"  + color;
            descriptionDiv.innerText = value + "%";
        }
        if (this.tooltipContainer.className.search("show") < 0) {
          this.tooltipContainer.className += " show";
        }
    }

    /**
     * Default hide tooltip
     *
     * @private
     */
    function hideTooltip() {
        var classNames = this.tooltipContainer.className.split(" "),
            position = classNames.indexOf("show");
        if (position >= 0) {
          classNames.splice(position, 1);
          this.tooltipContainer.className = classNames.join(" ");
        }
    }

    /**
     * Default move tooltip
     *
     * @param {Number} cursorX - cursor position by X
     * @param {Number} cursorY - cursor position by Y
     * @private
     */
    function moveTooltip(cursorX, cursorY) {
        var left = cursorX + this.cursorOffset.x,
            top = cursorY + this.cursorOffset.y;
        this.tooltipContainer.setAttribute("style", "top:" + top + "; left: " + left);
    }

    return {draw: draw};
});
